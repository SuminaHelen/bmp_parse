from unittest import main, TestCase
from bmp_parser import parse, CompressionType
from itertools import product


class ParserTestCase(TestCase):
    def test_white1(self):
        img = parse('white_cases/white.1.bmp')
        self.assertEqual(img.header["height"], 4)
        self.assertEqual(img.header["width"], 10)
        self.assertEqual(img.header["offset"], 62)
        self.assertEqual(img.header["bits_per_pixel"], 1)
        for i, j in product(range(10), range(4)):
            self.assertEqual(img.pixels[j][i], 1)

    def test_corner1(self):
        img = parse('cases/corner.1.bmp')
        self.assertEqual(img.header["height"], 2)
        self.assertEqual(img.header["width"], 3)
        self.assertEqual(img.header["offset"], 62)
        self.assertEqual(img.header["bits_per_pixel"], 1)
        black, white = 0, 1
        self.assertEqual(img.pixels, [
            (black, white, white),
            (white, white, black),
        ])

    def test_white4(self):
        img = parse('white_cases/white.4.bmp')
        self.assertEqual(img.header["height"], 4)
        self.assertEqual(img.header["width"], 10)
        self.assertEqual(img.header["offset"], 118)
        self.assertEqual(img.header["bits_per_pixel"], 4)
        self.assertEqual(img.header["compression_type"], CompressionType.NONE)
        for i, j in product(range(10), range(4)):
            self.assertEqual(img.pixels[j][i], 15)
            self.assertEqual(img.color_table[img.pixels[j][i]], (255, 255, 255, 0))

    def test_corner4(self):
        img = parse('cases/corner.4.bmp')
        self.assertEqual(img.header["height"], 2)
        self.assertEqual(img.header["width"], 3)
        self.assertEqual(img.header["offset"], 118)
        self.assertEqual(img.header["bits_per_pixel"], 4)
        self.assertEqual(img.header["compression_type"], CompressionType.NONE)
        red, white, green = 9, 15, 10
        self.assertEqual(img.pixels, [
            (red, white, white),
            (white, white, green)
        ])
        self.assertEqual(img.color_table[red], (255, 0, 0, 0))
        self.assertEqual(img.color_table[white], (255, 255, 255, 0))
        self.assertEqual(img.color_table[green], (0, 255, 0, 0))

    def test_white8(self):
        img = parse('white_cases/white.8.bmp')
        self.assertEqual(img.header["height"], 4)
        self.assertEqual(img.header["width"], 10)
        self.assertEqual(img.header["offset"], 1078)
        self.assertEqual(img.header["bits_per_pixel"], 8)
        self.assertEqual(img.header["compression_type"], CompressionType.NONE)
        for i, j in product(range(10), range(4)):
            self.assertEqual(img.pixels[j][i], 255)
        self.assertEqual(img.color_table[255], (255, 255, 255, 0))

    def test_corner8(self):
        img = parse('cases/corner.8.bmp')
        self.assertEqual(img.header["height"], 2)
        self.assertEqual(img.header["width"], 3)
        self.assertEqual(img.header["offset"], 1078)
        self.assertEqual(img.header["bits_per_pixel"], 8)
        self.assertEqual(img.header["compression_type"], CompressionType.NONE)
        red, white, green = 79, 255, 113
        self.assertEqual(img.pixels, [
            (red, white, white),
            (white, white, green)
        ])
        self.assertEqual(img.color_table[79], (224, 32, 64, 0))
        self.assertEqual(img.color_table[255], (255, 255, 255, 0))
        self.assertEqual(img.color_table[113], (32, 192, 64, 0))

    def test_white24(self):
        img = parse('white_cases/white.24.bmp')
        self.assertEqual(img.header["height"], 4)
        self.assertEqual(img.header["width"], 10)
        self.assertEqual(img.header["offset"], 54)
        self.assertEqual(img.header["bits_per_pixel"], 24)
        self.assertEqual(img.header["compression_type"], CompressionType.NONE)
        for i, j in product(range(10), range(4)):
            self.assertEqual(img.pixels[j][i], (255,) * 3)

    def test_corner24(self):
        img = parse('cases/corner.24.bmp')
        self.assertEqual(img.header["height"], 2)
        self.assertEqual(img.header["width"], 3)
        self.assertEqual(img.header["offset"], 54)
        self.assertEqual(img.header["bits_per_pixel"], 24)
        self.assertEqual(img.header["compression_type"], CompressionType.NONE)
        red, white, green = (237, 28, 36), (255, 255, 255), (34, 177, 76)
        self.assertEqual(img.pixels, [
            (red, white, white),
            (white, white, green)
        ])

    def test_rgb32(self):
        img = parse('cases/rgb32.bmp')
        self.assertEqual(img.header["height"], 64)
        self.assertEqual(img.header["width"], 127)
        self.assertEqual(img.header["offset"], 54)
        self.assertEqual(img.header["bits_per_pixel"], 32)
        self.assertEqual(img.header["compression_type"], CompressionType.NONE)
        the_reddest, redder, red = (255, 0, 0, 0), (255, 8, 8, 0), (255, 16, 16, 0)
        self.assertEqual(img.pixels[0][:3], (
            the_reddest, redder, red
        ))

    def kozel(self):
        img = parse('cases/kozel.bmp')
        self.assertEqual(img.header["height"], 512)
        self.assertEqual(img.header["width"], 512)
        self.assertEqual(img.header["offset"], 54)
        self.assertEqual(img.header["bits_per_pixel"], 24)
        self.assertEqual(img.header["compression_type"], CompressionType.NONE)

    def test_histogram(self):
        img = parse('white_cases/white.1.bmp')
        self.assertEqual(img.histogram, [[255]*40, [255]*40, [255]*40])
        img = parse('cases/corner.1.bmp')
        self.assertEqual(img.histogram, [[0, 255, 255, 255, 255, 0],
                                         [0, 255, 255, 255, 255, 0],
                                         [0, 255, 255, 255, 255, 0]])
        img = parse('cases/corner.4.bmp')
        self.assertEqual(img.histogram, [[255, 255, 255, 255, 255, 0],
                                         [0, 255, 255, 255, 255, 255],
                                         [0, 255, 255, 255, 255, 0]])
        img = parse('cases/corner.8.bmp')
        self.assertEqual(img.histogram, [[224, 255, 255, 255, 255, 32],
                                         [32, 255, 255, 255, 255, 192],
                                         [64, 255, 255, 255, 255, 64]])
        img = parse('cases/corner.24.bmp')
        self.assertEqual(img.histogram, [[237, 255, 255, 255, 255, 34],
                                         [28, 255, 255, 255, 255, 177],
                                         [36, 255, 255, 255, 255, 76]])


if __name__ == '__main__':
    main()
