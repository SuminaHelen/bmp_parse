from enum import IntEnum
from struct import unpack
import sys


class CompressionType(IntEnum):
    """
    announces different types of compression
    """
    NONE = 0
    RLE8 = 1
    RLE4 = 2
    BITFIELDS = 3
    JPEG = 4
    PNG = 5


class BitmapImage:
    """
    makes bmp image with fields
    """
    def __init__(self, pixels, hdr, color_table, histogram):
        self.header = {}
        (self.header["sig"], self.header["size"], self.header["reserved"],
         self.header["offset"], self.header["size2"], self.header["width"],
         self.header["height"], self.header["planes"], self.header["bits_per_pixel"],
         self.header["compression_type"], self.header["image_data_bytes"],
         self.header["hpixels_per_m"], self.header["vpixels_per_m"],
         self.header["color_table_size"], self.header["important_colors"]) = hdr
        self.header["compression_type"] = CompressionType(self.header["compression_type"])
        self.pixels = pixels
        self.color_table = color_table
        self.histogram = histogram


def bits_to_pixel(bits):
    """
    makes pixels from bits data
    """
    bc = len(bits)
    if bc in [1, 4, 8]:
        return int(bits, 2)
    elif bc == 32:
        b, g, r, a = bits[:8], bits[8:16], bits[16:24], bits[24:]
        return tuple(int(x, 2) for x in [r, g, b, a])


def bits24(bits):
    """
    if bits count is 24, makes pixels from bits data
    """
    b, g, r = bits[:8], bits[8:16], bits[16:]
    return tuple(int(x, 2) for x in [r, g, b])


def bytes_to_bits(bts):
    """
    translates bytes to bits
    """
    result = bin(int.from_bytes(bts, 'big'))[2:]
    return result.zfill(8 * len(bts))


def make_histogram(pixels, color_table, width, height, bpp):
    """
    calculates data to make a histogram
    """
    red, green, blue = [], [],[]
    for i in range(height):
        for j in range(width):
            if color_table:
                red.append(color_table[pixels[i][j]][0])
                green.append(color_table[pixels[i][j]][1])
                blue.append(color_table[pixels[i][j]][2])
            elif bpp >= 24:
                red.append(pixels[i][j][0])
                green.append(pixels[i][j][1])
                blue.append(pixels[i][j][2])
            elif bpp == 1:
                if pixels[i][j] == 1:
                    red.append(255)
                    green.append(255)
                    blue.append(255)
                else:
                    red.append(0)
                    green.append(0)
                    blue.append(0)
    return [red, green, blue]


def check_format(sig, reserved):
    """
    checks if filename's content is in bmp format
    """
    if sig == 19778 and reserved == 0:
        return
    else:
        print("That's not bmp")
        sys.exit(0)


def parse(filename):
    """
    parses bmp image
    """
    with open(filename, 'rb') as file:
        hdr = unpack('<hii iiii hh iiiiii', file.read(54))
        (sig, size, reserved, offset, size2, width,
         height, planes, bits_per_pixel, compression_type,
         image_data_bytes, hpixels_per_m, vpixels_per_m,
         colors, important_colors) = hdr
        check_format(sig, reserved)
        pixels = []
        color_table = []
        ct = file.read(offset - 54)  # skip
        bwidth = bits_per_pixel * width
        if compression_type == 0:
            b2p = bits24 if bits_per_pixel == 24 else bits_to_pixel
            for _ in range(height):
                bts = file.read(-4 * (bwidth // -32))
                bits = bytes_to_bits(bts)[:bwidth]
                row = range(0, bwidth, bits_per_pixel)
                pixels.append(tuple(b2p(bits[x:x+bits_per_pixel]) for x in row))
            pixels.reverse()
        if bits_per_pixel == 4 or bits_per_pixel == 8:
            for i in range(0, len(ct), 4):
                bits = bytes_to_bits(ct[i:i+4])
                color_table.append(bits_to_pixel(bits))
        histogram = make_histogram(pixels, color_table, width, height, bits_per_pixel)
        return BitmapImage(pixels, hdr, color_table, histogram)
