from PyQt5.QtWidgets import QApplication, QWidget, QPushButton
from PyQt5.QtGui import QPainter, QColor, QFont
import sys
import os
import glob
from bmp_parser import parse
from itertools import cycle
import matplotlib.pyplot as plt


class Viewer(QWidget):
    def __init__(self, images):
        """
        initializes fields of bmp to draw it
        """
        super().__init__()
        self.images = images
        self.bmps = []
        for image in images:
            self.bmps.append(parse(image))
        self.image_iter = iter(cycle(range(len(images))))
        self.bmp = self.bmps[next(self.image_iter)]
        self.init_ui()

    def init_ui(self):
        """
        initializes user interface
        """
        self.setGeometry(100, 100, 800, 600)
        self.setWindowTitle('BMP STRUCTURE')
        btn_next = QPushButton("==>", self)
        btn_next.move(600, 550)
        btn_next.clicked.connect(self.next_clicked)
        btn_histogram = QPushButton("RGB histogam", self)
        btn_histogram.move(600, 450)
        btn_histogram.clicked.connect(self.histogram_clicked)
        self.show()

    def next_clicked(self):
        """
        initializes the button which shows the next bmp
        """
        self.bmp = self.bmps[next(self.image_iter)]
        self.repaint()

    def histogram_clicked(self):
        """
        initializes the button which shows the histogram
        """
        bins = [i * 10 for i in range(27)]
        colors = ['red', 'green', 'blue']
        plt.hist(self.bmp.histogram, bins=bins, color=colors, label=colors)
        plt.xlabel('Color intensity')
        plt.ylabel('Pixels quantity')
        plt.title('RGB histogram')
        plt.legend(prop={'size': 10})
        plt.grid(True)
        plt.show()

    def paintEvent(self, e):
        qp = QPainter()
        qp.begin(self)
        self.draw_bmp(qp)
        self.draw_info(qp)
        qp.end()

    def draw_bmp(self, qp):
        for i in range(self.bmp.header["height"]):
            for j in range(self.bmp.header["width"]):
                if self.bmp.header["bits_per_pixel"] >= 24:
                    qp.setPen(QColor(*self.bmp.pixels[i][j][:3]))
                    qp.drawPoint(j, i)
                elif self.bmp.header["bits_per_pixel"] == 1:
                    if self.bmp.pixels[i][j]:
                        color = QColor(255, 255, 255)
                    else:
                        color = QColor(0, 0, 0)
                    qp.setPen(color)
                    qp.drawPoint(j, i)
                elif self.bmp.color_table:
                    color = self.bmp.color_table[self.bmp.pixels[i][j]][:3]
                    qp.setPen(QColor(*color))
                    qp.drawPoint(j, i)

    def draw_info(self, qp):
        qp.setPen(QColor("black"))
        qp.setFont(QFont('Decorative', 8, 50))
        i = iter(range(1, 30))
        for k, v in self.bmp.header.items():
            qp.drawText(self.bmp.header["width"] + 20, next(i)*10, k + ":  " + str(v))


if __name__ == '__main__':
    app = QApplication(sys.argv)
    directory = 'cases'
    os.chdir(directory)
    images = glob.glob("*.bmp")
    v = Viewer(images)
    sys.exit(app.exec_())